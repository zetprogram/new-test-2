<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2>студии и секции</h2>
		</div>
		<section class="sekcii-container">
			<div class="container">
				<div class="grid-3">
<?php
		$args = array(
	'taxonomy'     => 'category',
	'type'         => 'post',
	'orderby'      => 'id',
	'order'        => 'ASC',
	'hide_empty'   => false,
	'hierarchical' => false,
	'parent' => 0,
	'exclude'      => 1
	
);
 
$catlist = get_terms('category',$args);
?>
 <?php foreach ($catlist as $cat) : ?>
 					<div class="sekcii-item sekcii-1" style="background-image: url(<?php the_field('kartinka_kategorii', 'category_' . $cat->term_id);?>);">
						<a href="<?php echo get_term_link($cat->slug, 'category'); ?>"><?php echo $cat->name; ?></a>
					</div>
 <?php endforeach; ?>

				</div>
			</div>
		</section>
<?php get_footer();?>