	</main>
	<footer>
		<div class="banner-callback">
			<h2>у вас есть вопрос к нам?</h2>
			<a href="/kontakty/" class="gradient-btn">Обратная связь!</a>
		</div>
		<div class="content-footer">
			<div class="container">
				<div class="grid-3">
					<div class="content-footer_item footer-menu">
						<div class="footer-title-block">
							<h3 class="footer-title">Меню</h3>
							<hr>
						</div>
						<?php 
						wp_nav_menu( array(
						'menu_class'=>'menu',
    					'theme_location'=>'bottom'
						) );
				?>
					</div>
					<div class="content-footer_item contacts">
						<div class="footer-title-block">
							<h3 class="footer-title"><a href="tel:+74997251208"><i class="fas fa-phone-alt"></i> +7 (499) 725-12-08</a></h3>
							<hr>
						</div>
						<ul>
							<li><a href="mailto:planeta-nz@yandex.ru"><i class="fas fa-envelope"></i>Эл. почта: planeta-nz@yandex.ru</a></li>
							<li><a href="#"><i class="fas fa-map-marker"></i>г. Москва, ул. Кленовый бульвар, д.26</a></li>
							<li><a href="#"><i class="fas fa-clock"></i>Часы работы: 9:00 - 18:00</a></li>
						</ul>
					</div>
					<div class="content-footer_item logo">
						<div class="footer-title-block">
							<h3 class="footer-title">Учреждения</h3>
							<hr>
						</div>
						<ul>
							<li><a href="#"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/footer-logo1.png"></a></li>
							<li><a href="https://nagatinsky-zaton.mos.ru/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/footer-logo2.png"></a></li>
							<li><a href="https://uao.mos.ru/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/footer-logo3.png"></a></li>
						</ul>
						<div class="version">
							<!--<a href="#" class="white-btn">Версия для<br>
слабовидящих</a>-->
							<?php echo do_shortcode('[bvi text="Версия для <br>слабовидящих"]')?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<div class="scroll-to-top"></div>

	<div class="hidden">
		<div class="search_popup" id="search_popup">
			<?php get_search_form(); ?>
		</div>
		<div class="mob_menu_container" id="mob_menu">
			<div class="menu">
						<?php 
						wp_nav_menu( array(
						'menu_class'=>'menu',
    					'theme_location'=>'top'
						) );
				?>
			</div>
			<div class="social-link">
				<ul>
					<li class="facebook"><a href="https://vk.com/planeta_mol" target="_blank"><i class="fab fa-vk"></i></a></li>
					<li class="instagram"><a href="https://www.instagram.com/planetamolodykh/" target="_blank"><i class="fab fa-instagram"></i></a></li>
					<li class="youtube"><a href="https://www.youtube.com/channel/UCLZWJ66nFTNAWSp5gJ8eXsw" target="_blank"><i class="fab fa-youtube"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
	<!-- Google Analytics counter --><!-- /Google Analytics counter -->
	

	<?php wp_footer();?>
	<script type="text/javascript">
    // Функция ymaps.ready() будет вызвана, когда
    // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
    ymaps.ready(init);
    function init(){
        // Создание карты.
        var myMap = new ymaps.Map("map", {
            // Координаты центра карты.
            // Порядок по умолчанию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [55.676915, 37.677643],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.
            zoom: 13
        });
		myMap.geoObjects
        .add(new ymaps.Placemark([55.676915, 37.677643], {
            balloonContentHeader: '<strong>Россия, Москва, Кленовый бульвар, 26 </strong>',
            balloonContentBody: [
            	<?php $query = new WP_Query("tag_id=17");
        		if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
					'<a href="<?php the_permalink();?>"><?php the_title();?></a></br>',
				<?php endwhile;endif;?>
            ].join('')

        }, {
            preset: 'islands#educationCircleIcon',
            iconColor: 'red'
        }))
        .add(new ymaps.Placemark([55.683529, 37.679403], {
            balloonContentHeader: '<strong>Россия, Москва, Судостроительная улица, 27к2</strong>',
			balloonContentBody: [
            	<?php $query = new WP_Query("tag_id=18");
        		if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
					'<a href="<?php the_permalink();?>"><?php the_title();?></a></br>',
				<?php endwhile;endif;?>
            ].join('')
        }, {
            preset: 'islands#redEducationIcon',
			iconColor: '#198cff',
			iconCaptionMaxWidth: '20'
        }))
        .add(new ymaps.Placemark([55.677646, 37.675298], {
            balloonContentHeader: '<strong>Россия, Москва, улица Новинки, 25</strong>',
			balloonContentBody: [
            	<?php $query = new WP_Query("tag_id=19");
        		if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
					'<a href="<?php the_permalink();?>"><?php the_title();?></a></br>',
				<?php endwhile;endif;?>
            ].join('')
        }, {
            preset: 'islands#redEducationIcon',
			iconColor: '#198cff',
			iconCaptionMaxWidth: '20'
        }))
        .add(new ymaps.Placemark([55.676585, 37.692384], {
            balloonContentHeader: '<strong>Россия, Москва, Коломенская улица, 23к2</strong>',
			balloonContentBody: [
            	<?php $query = new WP_Query("tag_id=20");
        		if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
					'<a href="<?php the_permalink();?>"><?php the_title();?></a></br>',
				<?php endwhile;endif;?>
            ].join('')
        }, {
            preset: 'islands#redEducationIcon',
			iconColor: '#198cff',
			iconCaptionMaxWidth: '20'
        }))
        .add(new ymaps.Placemark([55.683590, 37.707134], {
            balloonContentHeader: '<strong>Россия, Москва, Коломенская набережная, 6к2</strong>',
			balloonContentBody: [
            	<?php $query = new WP_Query("tag_id=21");
        		if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
					'<a href="<?php the_permalink();?>"><?php the_title();?></a></br>',
				<?php endwhile;endif;?>
            ].join('')
        }, {
            preset: 'islands#redEducationIcon',
			iconColor: '#198cff',
			iconCaptionMaxWidth: '20'
        }))
        .add(new ymaps.Placemark([55.680717, 37.677445], {
            balloonContentHeader: '<strong>Россия, Москва, Судостроительная улица, 18к2</strong>',
			balloonContentBody: [
            	<?php $query = new WP_Query("tag_id=22");
        		if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
					'<a href="<?php the_permalink();?>"><?php the_title();?></a></br>',
				<?php endwhile;endif;?>
            ].join('')
        }, {
            preset: 'islands#redEducationIcon',
			iconColor: '#198cff',
			iconCaptionMaxWidth: '20'
        }))
        .add(new ymaps.Placemark([55.685016, 37.682332], {
            balloonContentHeader: '<strong>Россия, Москва, Якорная улица, 8к2</strong>',
			balloonContentBody: [
            	<?php $query = new WP_Query("tag_id=23");
        		if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
					'<a href="<?php the_permalink();?>"><?php the_title();?></a></br>',
				<?php endwhile;endif;?>
            ].join('')
        }, {
            preset: 'islands#redEducationIcon',
			iconColor: '#198cff',
			iconCaptionMaxWidth: '20'
        }))
        .add(new ymaps.Placemark([55.688913, 37.684542], {
            balloonContentHeader: '<strong>Россия, Москва, Нагатинская набережная, 54</strong>',
			balloonContentBody: [
            	<?php $query = new WP_Query("tag_id=24");
        		if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
					'<a href="<?php the_permalink();?>"><?php the_title();?></a></br>',
				<?php endwhile;endif;?>
            ].join('')
        }, {
            preset: 'islands#redEducationIcon',
			iconColor: '#198cff',
			iconCaptionMaxWidth: '20'
        }));
    }
</script>
</body>
</html>