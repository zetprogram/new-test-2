<?php
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');

show_admin_bar(false);

add_theme_support('post-thumbnails');

add_theme_support('menus');

//Добавление <title>:
add_theme_support( 'title-tag' );


function my_theme_load_resources() {

	wp_enqueue_style( 'owl', get_template_directory_uri() . '/libs/owl/assets/owl.carousel.css', array(), '0.1' );
	wp_enqueue_style( 'popup', get_template_directory_uri() . '/libs/magnific-popup/magnific-popup.css', array(), '0.1' );
	//wp_enqueue_style( 'slick', get_template_directory_uri() . '/libs/slick/slick-theme.css', array(), '0.1' );
	wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css', array(), '0.1' );
	wp_enqueue_style( 'media-css', get_template_directory_uri() . '/media.css', array(), '0.1' );


	wp_enqueue_script( 'jqueryjs', get_template_directory_uri() . '/libs/jquery/jquery-2.1.3.min.js', array(), '0.1', true );
	wp_enqueue_script( 'owljs', get_template_directory_uri() . '/libs/owl/owl.carousel.min.js', array(), '0.1', true );
	wp_enqueue_script( 'popupjs', get_template_directory_uri() . '/libs/magnific-popup/jquery.magnific-popup.min.js', array(), '0.1', true );
	wp_enqueue_script( 'yamaps', 'https://api-maps.yandex.ru/2.1/?apikey=41aed914-0a38-4488-9a29-81b6b9881efe&lang=ru_RU', array(), '0.1', true );
	wp_enqueue_script( 'commonjs', get_template_directory_uri() . '/js/common.js', array(), '0.1', true );

};
add_action( 'wp_enqueue_scripts', 'my_theme_load_resources' );

register_nav_menus(array(
	'top'    => 'Верхнее меню',    //Название месторасположения меню в шаблоне
	'bottom' => 'Нижнее меню'      //Название другого месторасположения меню в шаблоне
));

add_action( 'init', 'tpl_team' );
function tpl_team() {
	register_post_type( 'team', array(
		'public' => true,
		'labels' => array(
			'name' => 'Наша команда',
			'all_items' => 'Вся команда',
			'add_new' => 'Добавить человека',
			'add_new_item' => 'Добавление человека в команду'
			),
		'supports' => array( 'title', 'thumbnail' ),
		'taxonomies' => array( 'post_tag', 'category ')
		)
	);
};
add_action( 'init', 'tpl_gallery' );
function tpl_gallery() {
	register_post_type( 'gallery', array(
		'public' => true,
		'labels' => array(
			'name' => 'Галлерея',
			'all_items' => 'Галлерея',
			'add_new' => 'Добавить галлерею',
			'add_new_item' => 'Добавление галлереи'
			),
		'supports' => array( 'title', 'thumbnail', 'editor' ),
		'taxonomies' => array( 'post_tag', 'category ')
		)
	);
};
add_action( 'init', 'tpl_dostijenia' );
function tpl_dostijenia() {
	register_post_type( 'dostijenia', array(
		'public' => true,
		'labels' => array(
			'name' => 'Достижения',
			'all_items' => 'Все достижения',
			'add_new' => 'Добавить достижение',
			'add_new_item' => 'Добавление достижения'
			),
		'supports' => array( 'title', 'thumbnail', 'editor' ),
		'taxonomies' => array( 'post_tag', 'category ')
		)
	);
};
add_action( 'init', 'tpl_event' );
function tpl_event() {
	register_post_type( 'event', array(
		'public' => true,
		'labels' => array(
			'name' => 'Мероприятия',
			'all_items' => 'Все мероприятия',
			'add_new' => 'Добавить мероприятие',
			'add_new_item' => 'Добавление мероприятия'
			),
		'supports' => array( 'title', 'thumbnail', 'editor'),
		'taxonomies' => array( 'post_tag', 'category ')
		)
	);
};
add_action( 'init', 'tpl_docs' );
function tpl_docs() {
	register_post_type( 'docs', array(
		'public' => true,
		'labels' => array(
			'name' => 'Документы',
			'all_items' => 'Документы',
			'add_new' => 'Добавить документ',
			'add_new_item' => 'Добавление документа'
			),
		'supports' => array( 'title', 'thumbnail' ),
		'taxonomies' => array( 'post_tag', 'category ')
		)
	);
};
add_action( 'init', 'tpl_href' );
function tpl_href() {
	register_post_type( 'href', array(
		'public' => true,
		'labels' => array(
			'name' => 'Полезные ссылки',
			'all_items' => 'Полезные ссылки',
			'add_new' => 'Добавить ссылку',
			'add_new_item' => 'Добавление ссылки'
			),
		'supports' => array( 'title', 'thumbnail' ),
		'taxonomies' => array( 'post_tag', 'category ')
		)
	);
};