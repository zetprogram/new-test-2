<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2><?php the_title();?></h2>
		</div>
		<section class="docs-container">
			<div class="container">
				<div class="grid-2">

					<?php
//Вывод записей
$args = array(
	'post_type' => 'docs',
	'posts_per_page' => -1
);
$docs = new WP_Query( $args );
?>

<?php if( $docs->have_posts() ) : while ( $docs->have_posts() ) : $docs->the_post(); ?>


<div class="docs-item"><a href="<?php the_field('ssylka_na_dokument');?>"><?php the_title();?></a></div>
					

<?php endwhile; else: ?>

	<h3>Записей нет</h3>

<?php endif; ?>
					
				</div>
			</div>
		</section>
<?php get_footer();?>