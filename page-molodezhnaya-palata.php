<?php get_header();?>
<section class="molodej">
    <div class="container-1200">
        <div class="page-title-center left-align">
            <div class="hr-grad"></div>
            <h2><?php the_title();?> <span>района Нагатинский затон</span></h2>
        </div>
        <div class="contact_palata">
            <div class="grid-2">
                <div class="left-contact_palata">
                <p>Записаться в Молодёжную палату<br> района Нагатинский затон можно через наш сайт<br> или по телефону</p>
                <h2><a href="#">+7 (499) 725-12-08</a></h2>
                </div>
                <div class="href_contact_palata"><a href="/kontakty/" class="gradient-btn">Записаться!</a></div>
            </div>
        </div>
        <?php the_content();?>
        <div class="gallery-slider">
            <?php
    //Get the images ids from the post_metadata
    $images = acf_photo_gallery('gallereya', $post->ID);
    //Check if return array has anything in it
    if( count($images) ):
        //Cool, we got some data so now let's loop over it
        foreach($images as $image):
            $id = $image['id']; // The attachment id of the media
            $title = $image['title']; //The title
            $caption= $image['caption']; //The caption
            $full_image_url= $image['full_image_url']; //Full size image url
            $url= $image['url']; //Goto any link when clicked
            $target= $image['target']; //Open normal or new tab
            $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
            $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
?>
    
        <?php if( !empty($url) ){ ?><a href="<?php echo $url; ?>" <?php echo ($target == 'true' )? 'target="_blank"': ''; ?>><?php } ?>

            <a href="<?php echo $full_image_url; ?>" class="go_to_gallery"><div class="gallery_image" style="background-image: url(<?php echo $full_image_url; ?>);"></div> </a>     

        <?php if( !empty($url) ){ ?></a><?php } ?>
   
<?php endforeach; endif; ?>
        </div>
    </div>
</section>
<?php get_footer();?>