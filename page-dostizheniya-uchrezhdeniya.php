<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2><?php the_title();?></h2>
		</div>
		<section class="gallery-container">
			<div class="container">
				<div class="grid-gallery">
					<?php
//Вывод записей
$args = array(
	'post_type' => 'dostijenia',
	'posts_per_page' => -1
);
$dostijenia = new WP_Query( $args );
?>

<?php if( $dostijenia->have_posts() ) : while ( $dostijenia->have_posts() ) : $dostijenia->the_post(); ?>
<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );?>

<div class="gallery-image" style="background-image: url(<?php echo $large_image_url[0];?>); "><a href="<?php echo $large_image_url[0];?>" class="popup-image"><span>+</span></a></div>
					<div class="gallery-text">
						<h3><?php the_title();?></h3>
						<?php the_content();?>
					</div>



<?php endwhile; else: ?>

	<h3>Записей нет</h3>

<?php endif; ?>
					
				</div>
			</div>
		</section>
<?php get_footer();?>