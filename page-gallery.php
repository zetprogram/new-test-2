<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2><?php the_title();?></h2>
		</div>
		<section class="gallery-container">
			<div class="container">
				
			<?php $id = $_GET["id"]; ?>

			<div class="grid-gallery" style="margin-bottom:20px;">
					<?php

					//Вывод записей
					$args = array(
						'post_type' => 'gallery',
						'posts_per_page' => -1,
						'post__in' => array($id),
					);
					$gallery = new WP_Query( $args );
					?>

					<?php if( $gallery->have_posts() ) : while ( $gallery->have_posts() ) : $gallery->the_post(); ?>
					<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );?>

					<div class="gallery-image" style="background-image: url(<?php echo $large_image_url[0];?>); "><a href="<?php the_permalink(); ?>"><span>+</span></a></div>
										<div class="gallery-text">
											<h3><?php the_title();?></h3>
											<?php the_content();?>
											<a href="<?php the_permalink(); ?>" class="gradient-btn">Галерея фото</a>
										</div>



					<?php endwhile; else: ?>


					<?php endif; ?>
				</div>
			
				<div class="grid-gallery">
					<?php

					//Вывод записей
					$args = array(
						'post_type' => 'gallery',
						'posts_per_page' => -1,
						'post__not_in' => array($id),

					);
					$gallery = new WP_Query( $args );
					?>

					<?php if( $gallery->have_posts() ) : while ( $gallery->have_posts() ) : $gallery->the_post(); ?>
					<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );?>

					<div class="gallery-image" style="background-image: url(<?php echo $large_image_url[0];?>); "><a href="<?php the_permalink(); ?>"><span>+</span></a></div>
										<div class="gallery-text">
											<h3><?php the_title();?></h3>
											<?php the_content();?>
											<a href="<?php the_permalink(); ?>" class="gradient-btn">Галерея фото</a>
										</div>



					<?php endwhile; else: ?>

						<h3>Записей нет</h3>

					<?php endif; ?>
				</div>
			</div>
		</section>

<?php get_footer();?>