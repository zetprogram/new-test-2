<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2><?php the_title();?></h2>
		</div>
		<section class="href-container">
			<div class="container">
				<div class="grid-3">
					
<?php
//Вывод записей
$args = array(
	'post_type' => 'href',
	'posts_per_page' => -1
);
$href = new WP_Query( $args );
?>

<?php if( $href->have_posts() ) : while ( $href->have_posts() ) : $href->the_post(); ?>

<div class="href-item" style="background-image: url(<?php
$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );
echo $large_image_url[0];
?>);">
						<a href="<?php the_field('ssylka_na_resurs');?>"><?php the_title();?></a>

					</div>

<?php endwhile; else: ?>

	<h3>Записей нет</h3>

<?php endif; ?>


					
				</div>
			</div>
		</section>
<?php get_footer();?>