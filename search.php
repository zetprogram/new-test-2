<?php
get_header();
?>

<?php if ( have_posts() ) : ?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2>Результат поиска по запросу: <?php echo get_search_query(); ?></h2>
		</div>
		<section class="sekcii-container">
			<div class="container">
				<div class="grid-3">

<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post();
?>
				<?$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );?>

		<div class="sekcii-item sekcii-<?php the_ID(); ?>" style="background-image: url(<?php echo $large_image_url[0];?>);">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</div>

				<?
			endwhile;?>
			</div>
			</div>
		</section>
		<? else :?>
			<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2>Результат поиска по запросу: <?php echo get_search_query(); ?></h2>
		</div>
		<section class="sekcii-container">
			<div class="container">
				<h2 style="text-align:center;">К сожалению ничего не найдено, попробуйте еще раз</h2>
			</div>
		</section>

		<?php endif;
		?>

<?php
get_footer();
?>