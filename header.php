<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="shortcut icon" href="favicon.png" />
	<script src="https://kit.fontawesome.com/be86b01a20.js" crossorigin="anonymous"></script>
	<?php wp_head();?>
</head>
<body>
	<header>
		<div class="top-bar">
			<div class="container">
				<div class="top-bar_items">
				<div class="grid-3">
					<div class="address">
						<i class="fas fa-map-marker"></i>г. Москва, ул. Кленовый Бульвар, д.26
					</div>
					<div class="contacts">
						<a href="mailto:planeta-nz@yandex.ru"><i class="fas fa-envelope"></i>Эл. почта: planeta-nz@yandex.ru</a>
						<a href="tal:84997251208"><i class="fas fa-phone-alt"></i>Конт. тел.: +7 (499) 725-12-08</a>
					</div>
					<div class="time-work">
						<i class="fas fa-clock"></i>Часы работы: 9:00 - 18:00
					</div>
				</div>
				</div>
			</div>
		</div>
		<div class="center-bar">
			<div class="container">
				<div class="center-bar_items">
					<div class="logo">
						<a href="/" alt="logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png"></a>
					</div>
					<div class="menu">
						<?php 
						wp_nav_menu( array(
						'menu_class'=>'menu',
    					'theme_location'=>'top'
						) );
				?>
					</div>
					<div class="social-link">
						<ul>
							<li class="facebook"><a href="https://vk.com/planeta_mol" target="_blank"><i class="fab fa-vk"></i></a></li>
							<li class="instagram"><a href="https://www.instagram.com/planetamolodykh/" target="_blank"><i class="fab fa-instagram"></i></a></li>
							<li class="youtube"><a href="https://www.youtube.com/channel/UCLZWJ66nFTNAWSp5gJ8eXsw" target="_blank"><i class="fab fa-youtube"></i></a></li>
						</ul>
					</div>
					<div class="seach_menu">
					<div class="search">
						<a href="#search_popup"><i class="fas fa-search"></i></a>
					</div>
					<div class="mob_menu">
						<a href="#mob_menu"><i class="fas fa-bars"></i></a>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="banner">
			<img src="/wp-content/uploads/2021/01/site.png">
		</div>
	</header>
	<main>