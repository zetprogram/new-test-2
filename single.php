<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2><?php the_title();?></h2>
		</div>
		<div class="single-container">
			<div class="container">
<?php
	if( have_posts() ){while( have_posts() ){the_post();?>
<section>
         <?php if( has_post_thumbnail() ) {?>
            <div class="grid-2 single-one content-single"> 
            <div class="image_curses"><?php echo get_the_post_thumbnail();?></div>
            <div class="content">
                <?php the_content(); ?>
            </div>
            </div>
            <?php }else{ ?>
            <div class="grid-1 single-one content-single"> 
            <div class="content">
                <?php the_content(); ?>
            </div>
            </div>
            <?php };?>
        
		</section>
        <section>
        <?php if(get_field('fotografiya_pedagoga')){?>
        <div class="grid-2 single-two content-single">
            <div class="content">
                <h5>Педагог</h5>
                <h3><?php the_field("imya_pedagoga")?></h3>
                <?php the_field("o_pedagoge");?>
            </div>
            <div class="image_curses"><img src="<?php the_field('fotografiya_pedagoga');?>"></div>
        </div>
    <?php }else{ ?>
        <div class="grid-1 single-two content-single" style="text-align: center;">
            <div class="content">
                <h5>Педагог</h5>
                <h3><?php the_field("imya_pedagoga")?></h3>
                <?php the_field("o_pedagoge");?>
            </div>
        </div>
    <?php };?>
</section>
<section class="info-single">
<div class="grid-2">
            <div class="contacts-item">
                        <div class="contacts-item_icon"><i class="fas fa-users"></i></div>
                        <div class="contacts-item_text">
                            Возраст:
                            <h4><?php the_field("vorzrast");?></h4>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item_icon"><i class="fas fa-wallet"></i></div>
                        <div class="contacts-item_text">
                            Стоимость:
                            <h4><?php the_field("stoimost");?></h4>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item_icon"><i class="fas fa-map-marker" aria-hidden="true"></i></div>
                        <div class="contacts-item_text">
                            Адрес:
                            <h4><?php the_field("adres");?></h4>
                        </div>  
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item_icon"><i class="fas fa-calendar-alt"></i></div>
                        <div class="contacts-item_text">
                            Расписание:
                            <h4><?php the_field("raspisanie");?></h4>
                        </div>  
                    </div>
                </div>
</section>
<section>
<div class="gallery-slider">
            <?php
    //Get the images ids from the post_metadata
    $images = acf_photo_gallery('gallereya', $post->ID);
    //Check if return array has anything in it
    if( count($images) ):
        //Cool, we got some data so now let's loop over it
        foreach($images as $image):
            $id = $image['id']; // The attachment id of the media
            $title = $image['title']; //The title
            $caption= $image['caption']; //The caption
            $full_image_url= $image['full_image_url']; //Full size image url
            $url= $image['url']; //Goto any link when clicked
            $target= $image['target']; //Open normal or new tab
            $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
            $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
?>
    
        <?php if( !empty($url) ){ ?><a href="<?php echo $url; ?>" <?php echo ($target == 'true' )? 'target="_blank"': ''; ?>><?php } ?>

            <a href="<?php echo $full_image_url; ?>" class="go_to_gallery"><div class="gallery_image" style="background-image: url(<?php echo $full_image_url; ?>);"></div> </a>     

        <?php if( !empty($url) ){ ?></a><?php } ?>
   
<?php endforeach; endif; ?>
        </div>
</section>
		<?php
	}
}
// постов нет
else {
	echo "<h2>Записей нет.</h2>";
}?>

				</div>
			</div>
		</div>
<?php get_footer();?>