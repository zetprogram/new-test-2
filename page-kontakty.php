<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2>ГБУ г. Москвы "Центр досуга "Планета молодых"</h2>
		</div>
		<section class="contacts-container">
			<div class="container">
				<div class="grid-3">
					<div class="contacts-item">
						<div class="contacts-item_icon phone-mail"><i class="fas fa-envelope" aria-hidden="true"></i>/<i class="fas fa-phone-alt" aria-hidden="true"></i></div>
						<div class="contacts-item_text">
							Свяжитесь с нами сейчас:
							<h4><a href="tel:+74997251208">+7 (499) 725-12-08</a></h4>
							<h4><a href="mailto:planeta-nz@yandex.ru">planeta-nz@yandex.ru</a></h4>
						</div>
					</div>
					<div class="contacts-item">
						<div class="contacts-item_icon"><i class="fas fa-map-marker" aria-hidden="true"></i></div>
						<div class="contacts-item_text">
								Место:
								<h4>г. Москва<br>ул. Кленовый Бульвар, д.26</h4>
						</div>
					</div>
					<div class="contacts-item">
						<div class="contacts-item_icon"><i class="fas fa-clock" aria-hidden="true"></i></div>
						<div class="contacts-item_text">
							Часы работы:
							<h4>9:00 - 18:00</h4>
						</div>	
					</div>
				</div>
			</div>
		</section>
		<section class="contacts-maps-callback">
			<div class="container">
				<div class="grid-2">
					<div class="contacts-map">
						<div id="map"></div>
					</div>
					<div class="contacts-callback">
						<?php echo do_shortcode('[contact-form-7 id="12" title="Обратная связь Контакты"]');?>
					</div>
				</div>
			</div>
		</section>
<?php get_footer();?>