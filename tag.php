<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2><?php single_tag_title();?></h2>
		</div>
		<section class="sekcii-container">
			<div class="container">
				<div class="grid-3">
<?php

	if( have_posts() ){
	// перебираем все имеющиеся посты и выводим их
	while( have_posts() ){
		the_post();
		?>
<?$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );?>

		<div class="sekcii-item sekcii-<?php the_ID(); ?>" style="background-image: url(<?php echo $large_image_url[0];?>);">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</div>

		<?php
	}
}
// постов нет
else {
	echo "<h2>Записей нет.</h2>";
}?>

				</div>
			</div>
		</section>
<?php get_footer();?>
