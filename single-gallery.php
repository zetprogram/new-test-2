<?php get_header();?>
<section class="molodej">
    <div class="container">
        <div class="page-title-center">
            <div class="hr-grad"></div>
            <h2><?php the_title();?></h2>
        </div>
        <div class="grid-4">
            <?php
    //Get the images ids from the post_metadata
    $images = acf_photo_gallery('gallereya', $post->ID);
    //Check if return array has anything in it
    if( count($images) ):
        //Cool, we got some data so now let's loop over it
        foreach($images as $image):
            $id = $image['id']; // The attachment id of the media
            $title = $image['title']; //The title
            $caption= $image['caption']; //The caption
            $full_image_url= $image['full_image_url']; //Full size image url
            $url= $image['url']; //Goto any link when clicked
            $target= $image['target']; //Open normal or new tab
            $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
            $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
?>
    
        <?php if( !empty($url) ){ ?><a href="<?php echo $url; ?>" <?php echo ($target == 'true' )? 'target="_blank"': ''; ?>><?php } ?>

            <a href="<?php echo $full_image_url; ?>" class="go_to_gallery"><div class="gallery_image" style="background-image: url(<?php echo $full_image_url; ?>);"></div> </a>     

        <?php if( !empty($url) ){ ?></a><?php } ?>
   
<?php endforeach; endif; ?>
        </div>
    </div>
</section>
<?php get_footer();?>