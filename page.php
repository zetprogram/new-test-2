<?php get_header();?>
<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2><?php the_title();?></h2>
			<section>
				<div class="container">
					<?php the_content();?>
				</div>
			</section>
		</div>
<?php get_footer();?>