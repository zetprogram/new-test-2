<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2><?php the_title();?></h2>
		</div>
		<section class="docs-priema-container">
			<div class="container">
					<h4 class="obshaya">Общая информация</h4>
				<div class="grid-3">
					<div class="docs-priema-text">
						<p>Какие документы необходимые документы для зачисления в учреждение <b>(для несовершеннолетних)</b>:<br>
•	Заявление (заполняется от руки);<br>
•	Договор об оказании услуг (заполняется от руки);<br>
•	Ксерокопия паспорта родителя/законного представителя ребенка (Первый лист и лист с пропиской);<br>
•	Ксерокопия свидетельства о рождении занимающегося (для лиц, в возрасте до 14 лет);<br>
•	Ксерокопия паспорта занимающегося (по достижении 14-летнего возраста);<br>
•	Оригинал медицинской справки от педиатра о группе здоровья (только для спортивных секций);<br>
•	Ксерокопия СНИЛС.
</p>
						<br>
<p>
Необходимые документы для зачисления в учреждение <b>(для совершеннолетних)</b>:<br>
•	Заявление (заполняется от руки);<br>
•	Договор об оказании услуг (заполняется от руки);<br>
•	Ксерокопия паспорта (Первый лист и лист с пропиской);<br>
•	Оригинал медицинской справки от педиатра о группе здоровья (только для спортивных секций);<br>
•	Ксерокопия СНИЛС.

</p>
					</div>
    <div class="tabs">
        <span class="tab"><h4>пакет документов <span>бюджет</span></h4></span>
        <span class="tab"><h4>пакет документов <span>внебюджет до 18 лет</span></h4></span>
        <span class="tab"><h4>пакет документов <span>внебюджет от 18 лет</span></h4></span>        
    </div>
    <div class="tab_content">
        <div class="tab_item">
        	<div class="title_tab"><h5><b>Образец</b> заполнения документов <span>бюджет</span></h5></div>
        	<div class="gallery_tab">
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/1.jpeg);"></div></div>
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/2.jpeg);"></div></div>
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/3.jpeg);"></div></div>
        	</div>
        	<div class="downloads_docs"><a href="/wp-content/uploads/2021/02/paket-dokumentov-byudzhet.zip"  download="" class="gradient-btn">Скачать пакет <br>документов</a></div>
        </div>
        <div class="tab_item">
        	<div class="title_tab"><h5><b>Образец</b> заполнения документов <span>внебюджет до 18 лет</span></h5></div>
        	<div class="gallery_tab">
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/4.jpeg);"></div></div>
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/5.jpeg);"></div></div>
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/6.jpeg);"></div></div>
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/7.jpeg);"></div></div>
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/8.jpeg);"></div></div>
        	</div>
        	<div class="downloads_docs"><a href="/wp-content/uploads/2021/02/paket-dokumentov-vnebyudzhet-s-18-let.zip"  download="" class="gradient-btn">Скачать пакет <br>документов</a></div>
        </div>
        <div class="tab_item">
        	<div class="title_tab"><h5><b>Образец</b> заполнения документов <span>внебюджет от 18 лет</span></h5></div>
        	<div class="gallery_tab">
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/9.jpeg);"></div></div>
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/10.jpeg);"></div></div>
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/11.jpeg);"></div></div>
        		<div class="gallery_tab_item"><div class="gallery_tab_image" style="background-image: url(/wp-content/uploads/2021/02/12.jpeg);"></div></div>
        	</div>
        	<div class="downloads_docs"><a href="/wp-content/uploads/2021/02/paket-dokumentov-do-18-let-vnebyudzhet.zip"  download="" class="gradient-btn">Скачать пакет <br>документов</a></div>
        </div>
    </div>
				
				</div>
			</div>
		</section>
<?php get_footer();?>