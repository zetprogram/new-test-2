<?php get_header();?>
		<section class="homepage">
			<div class="container">
				<div class="grid-2">
					<div class="homeslider">
						<div class="slide">
							<div class="grid-2">
				<?php
//Вывод записей
$args = array(
	'post_type' => 'event',
	'posts_per_page' => -1
);
$event = new WP_Query( $args );
$i=1;
?>
<?php if( $event->have_posts() ) : while ( $event->have_posts() ) : $event->the_post(); ?>
<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );?>

<?php 
				$date_now = strtotime("now");
				$date_last = get_field('data_nachala');
				$date_close = strtotime('+ 0 minutes', $date_last);
				if($date_now < $date_close){?>
					<?php if ( get_field( 'afisha' ) ): ?>

				<div class="event-item afisha_item">
					<div class="event-item-image" style="background-image: url(<?php echo $large_image_url[0];?>);">
						<a href="<?php the_permalink();?>"><span>+</span></a>
					</div>
				</div>
<?php if($i%3==0)
    {
        echo "</div>
						</div>
						<div class='slide'>
							<div class='grid-2'>";
    }
    $i++;?>
<?php endif; // end of if field_name logic ?>
					
				<?};?>
<?php endwhile; else: ?>
	<h3>Записей нет</h3>
<?php endif; ?>
</div>
			</div>
			</div>
					<div class="homepage-text">
						<div class="homepage-text-about">
							<div class="hr-grad-soc">
								<div class="hr-grad"></div>
								<div class="social-link">
									<ul>
										<li class="facebook"><a href="https://vk.com/planeta_mol" target="_blank"><i class="fab fa-vk"></i></a></li>
										<li class="instagram"><a href="https://www.instagram.com/planetamolodykh/" target="_blank"><i class="fab fa-instagram"></i></a></li>
										<li class="youtube"><a href="https://www.youtube.com/channel/UCLZWJ66nFTNAWSp5gJ8eXsw" target="_blank"><i class="fab fa-youtube"></i></a></li>
									</ul>
								</div>
							</div>
							<h2>Центр досуга <span>"Планета молодых"</span></h2>
							<p>ЦД «Планета молодых» - это не просто государственное бюджетное учреждение, это команда профессионалов, которые любят свой район и работают для населения района Нагатинский затон.</p>
						</div>
						<div class="homepage-text-contact">
							<h2>наши филиалы<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/arrow-to-map.png"></h2>
							<!--<div class="maps"><iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A2f163907ee8613cb3683a7f35b680008c022d2b432dbda0b75790f61b0e9977e&amp;source=constructor" width="100%" height="745" frameborder="0"></iframe></div>-->
							<div id="map"></div>
						</div>
					</div>
				</div>	
			</div>
		</section>
<?php get_footer();?>


