<?php get_header();?>

		<section class="single-event">
			<div class="container">
                <div class="grid-2">
                
                <?php
    if( have_posts() ){while( have_posts() ){the_post();?>
        <?php $arr = [  'января',  'февраля',  'марта',  'апреля',  'мая',  'июня',  'июля',  'августа',  'сентября',  'октября',  'ноября',  'декабря'];
         $date_last = get_field('data_nachala');
         $month = date('n' , $date_last)-1;
         $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );
         ?>
        <div class="photo_event"><a href="<?php echo $large_image_url[0];?>" class="popup-image"><?php echo get_the_post_thumbnail();?></a></div>
        <div class="content-event">
            <div class="date"><i class="far fa-calendar-alt"></i><span><?php echo  date("d" , $date_last).' '.$arr[$month].' , '. date("Y" , $date_last);?></span></div>
            <h1><?php the_title();?></h1>
            <?php the_content(); ?>
        </div>
        

        <?php
    }
}
// постов нет
else {
    echo "<h2>Записей нет.</h2>";
}?>    
                
				</div>
			</div>
		</section>


<?php get_footer();?>