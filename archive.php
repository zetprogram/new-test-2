<?php get_header();?>
		<div class="page-title-center">
			<div class="hr-grad"></div>
			<h2>студии и секции</h2>
		</div>
		<section class="sekcii-container">
			<div class="container">
				<div class="grid-3">
<?php
$parent_id = get_query_var('cat');
		$args = array(
	'taxonomy'     => 'category',
	'type'         => 'post',
	'orderby'      => 'id',
	'order'        => 'ASC',
	'hide_empty'   => false,
	'hierarchical' => false,
	'parent' => $parent_id,
	'exclude'      => 1
	
);
 
$catlist = get_categories($args);
?>
 <?php
$k=1;
if( $catlist ){
  foreach ($catlist as $cat) : ?>
 					<div class="sekcii-item sekcii-<?php echo $cat->term_id; ?>" style="background-image: url(<?php the_field('kartinka_kategorii', 'category_' . $cat->term_id);?>);">
						<a href="<?php echo get_term_link($cat->slug, 'category'); ?>"><?php echo $cat->name; ?></a>
					</div>
 <?php endforeach; 
}else{
	if( have_posts() ){
	// перебираем все имеющиеся посты и выводим их
	while( have_posts() ){
		the_post();
		?>
<?$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );?>

		<div class="sekcii-item sekcii-<?php the_ID(); ?>" style="background-image: url(<?php echo $large_image_url[0];?>);">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</div>

		<?php
	}
}
// постов нет
else {
	echo "<h2>Записей нет.</h2>";
}
}?>

				</div>
			</div>
		</section>
<?php get_footer();?>