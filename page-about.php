<?php get_header();?>
		<section class="about">
			<div class="container">
				<div class="grid-2">
					<div class="about-text-image">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/about-page-image.png">
						</div>
					<div class="about-text">
							<div class="hr-grad-soc">
								<div class="hr-grad"></div>
								<div class="social-link">
									<ul>
										<li class="facebook"><a href="https://vk.com/planeta_mol" target="_blank"><i class="fab fa-vk"></i></a></li>
										<li class="instagram"><a href="https://www.instagram.com/planetamolodykh/" target="_blank"><i class="fab fa-instagram"></i></a></li>
										<li class="youtube"><a href="https://www.youtube.com/channel/UCLZWJ66nFTNAWSp5gJ8eXsw" target="_blank"><i class="fab fa-youtube"></i></a></li>
									</ul>
								</div>
							</div>
							<h2>Центр досуга <span>"Планета молодых"</span></h2>
							<?php the_content();?>
					</div>
				</div>	
			</div>
		</section>
		<section class="team">
			<div class="container">
				<div class="page-title-center">
					<div class="hr-grad"></div>
					<h2>Наша команда</h2>
				</div>
				<div class="grid-4">

<?php
//Вывод записей
$args = array(
	'post_type' => 'team',
	'posts_per_page' => -1,
	'orderby' => 'id',
	'order' => 'ASC'
);
$team = new WP_Query( $args );
?>

<?php if( $team->have_posts() ) : while ( $team->have_posts() ) : $team->the_post(); ?>


<div class="about-team-item">
						<div class="about-team-item-img">
							<?php echo get_the_post_thumbnail();?>
						</div>
						<div class="about-team-item-text">
							<h4><?php the_title();?></h4>
							<p class="doljnost"><?php the_field("dolzhnost");?></p>
							<div class="phone_team">
								<div class="phone_team_icon"><i class="fas fa-phone-alt"></i></div>
								<div class="phone_team_number"><a href='tel:<?php the_field("telefon");?>'><?php the_field("telefon");?></a></div>
							</div>
						</div>
					</div>


<?php endwhile; else: ?>

	<h3>Записей нет</h3>

<?php endif; ?>
				</div>	
			</div>
		</section>
		<section class="about-href">
			<div class="container">
				<div class="grid-4">
					<div class="about-href-item">
						<a href="/poleznye-ssylki/">ПОЛЕЗНЫЕ<br>ССЫЛКИ</a>
					</div>
					<div class="about-href-item">
						<a href="/dokumenty/">ДОКУМЕНТЫ</a>
					</div>
					<div class="about-href-item">
						<a href="/dostizheniya-uchrezhdeniya/">ДОСТИЖЕНИЯ<br>УЧРЕЖДЕНИЯ</a>
					</div>
					<div class="about-href-item">
						<a href="/molodezhnaya-palata/">МОЛОДЕЖНАЯ<br>ПАЛАТА</a>
					</div>
				</div>	
			</div>
		</section>
<?php get_footer();?>