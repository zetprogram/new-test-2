<?php get_header();?>
<section>
	<div class="container">
		<div class="page-title-center new-event">
			<div class="hr-grad"></div>
			<h2>предстоящие мероприятия</h2>
		</div>
			<div class="event-slider">
				<?php
//Вывод записей
$args = array(
	'post_type' => 'event',
	'posts_per_page' => -1
);
$event = new WP_Query( $args );
?>
<?php if( $event->have_posts() ) : while ( $event->have_posts() ) : $event->the_post(); ?>
<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );?>

<?php 
				$arr = [  'января',  'февраля',  'марта',  'апреля',  'мая',  'июня',  'июля',  'августа',  'сентября',  'октября',  'ноября',  'декабря'];
				$date_now = strtotime("now");
				$date_last = get_field('data_nachala');
				$date_close = strtotime('+ 0 minutes', $date_last);
				$month = date('n' , $date_last)-1;
				if($date_now < $date_close){?>
					<?php if ( get_field( 'afisha' ) ): ?>

				<div class="event-item afisha_item">
					<div class="event-item-image" style="background-image: url(<?php echo $large_image_url[0];?>);">
						<a href="<?php the_permalink();?>"><span>+</span></a>
					</div>
				</div>

				<?php else: // field_name returned false ?>

				<div class="event-item">
					<div class="event-item-image" style="background-image: url(<?php echo $large_image_url[0];?>);">
						<a href="<?php the_permalink();?>"><span>+</span></a>
					</div>
					<div class="event-item-text">
						<div class="date">
							<i class="far fa-calendar-alt"></i><span><?php echo  date("d" , $date_last).' '.$arr[$month].' , '. date("Y" , $date_last);?></span>
						</div>
						<div class="title">
							<h4><?php the_title();?></h4>
						</div>
						<div class="href">
							<a href="<?php the_permalink();?>">Подробнее</a>
						</div>
					</div>
				</div>

<?php endif; // end of if field_name logic ?>
					
				<?};?>
<?php endwhile; else: ?>
	<h3>Записей нет</h3>
<?php endif; ?>
			</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="page-title-center old-event">
			<div class="hr-grad"></div>
			<h2>прошедшие мероприятия</h2>
		</div>
			<div class="event-slider">
				<?php
//Вывод записей
$args = array(
	'post_type' => 'event',
	'posts_per_page' => -1
);
$event = new WP_Query( $args );
?>
<?php if( $event->have_posts() ) : while ( $event->have_posts() ) : $event->the_post(); ?>
<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '' );?>

<?php 
				$arr = [  'января',  'февраля',  'марта',  'апреля',  'мая',  'июня',  'июля',  'августа',  'сентября',  'октября',  'ноября',  'декабря'];
				$date_now = strtotime("now");
				$date_last = get_field('data_nachala');
				$date_close = strtotime('+ 0 minutes', $date_last);
				$month = date('n' , $date_last)-1;
				if($date_now > $date_close){?>
					<div class="event-item">
					<div class="event-item-image" style="background-image: url(<?php echo $large_image_url[0];?>);">
						<?php $ssylka_na_gallereyu = get_field('ssylka_na_gallereyu');
						if($ssylka_na_gallereyu){?>
							<a href="<?php echo $ssylka_na_gallereyu;?>"><span>+</span></a>
						<?}else{?>
						<a href="/wp/gallery/?id=<?php the_field("id_galerei") ?>"><span>+</span></a>
						<?php };?>
					</div>
					<div class="event-item-text">
						<div class="date">
							<i class="far fa-calendar-alt"></i><span><?php echo  date("d" , $date_last).' '.$arr[$month].' , '. date("Y" , $date_last);?></span>
						</div>
						<div class="title">
							<h4><?php the_title();?></h4>
						</div>
						<div class="href">
							<?php if($ssylka_na_gallereyu){?>
							<a href="<?php echo $ssylka_na_gallereyu;?>">Подробнее</a>
						<?}else{?>
							<a href="/wp/gallery/?id=<?php the_field("id_galerei") ?>">Подробнее</a>
						<?php };?>
						</div>
					</div>
				</div>
				<?};?>
<?php endwhile; else: ?>
	<h3>Записей нет</h3>
<?php endif; ?>
			</div>
	</div>
</section>
<?php get_footer();?>